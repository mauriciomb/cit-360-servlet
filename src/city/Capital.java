package city;

import java.util.HashMap;
import java.util.Map;

public class Capital {

    static Map<String, String> stateCodeToCapitalMap = new HashMap();

    static {
        stateCodeToCapitalMap.put("AK", "Juneau");
        stateCodeToCapitalMap.put("AL", "Montgomery");
        stateCodeToCapitalMap.put("AR", "Little Rock");
        stateCodeToCapitalMap.put("AZ", "Phoenix");
        stateCodeToCapitalMap.put("CA", "Sacramento");
        stateCodeToCapitalMap.put("CO", "Denver");
        stateCodeToCapitalMap.put("CT", "Hartford");
        stateCodeToCapitalMap.put("DE", "Dover");
        stateCodeToCapitalMap.put("FL", "Tallahassee");
        stateCodeToCapitalMap.put("GA", "Atlanta");
        stateCodeToCapitalMap.put("HI", "Honolulu");
        stateCodeToCapitalMap.put("IA", "Des Moines");
        stateCodeToCapitalMap.put("ID", "Boise");
        stateCodeToCapitalMap.put("IL", "Springfield");
        stateCodeToCapitalMap.put("IN", "Indianapolis");
        stateCodeToCapitalMap.put("KS", "Topeka");
        stateCodeToCapitalMap.put("KY", "Frankfort");
        stateCodeToCapitalMap.put("LA", "Baton Rouge");
        stateCodeToCapitalMap.put("MA", "Boston");
        stateCodeToCapitalMap.put("MD", "Annapolis");
        stateCodeToCapitalMap.put("ME", "Augusta");
        stateCodeToCapitalMap.put("MI", "Lansing");
        stateCodeToCapitalMap.put("MN", "St. Paul");
        stateCodeToCapitalMap.put("MO", "Jefferson City");
        stateCodeToCapitalMap.put("MS", "Jackson");
        stateCodeToCapitalMap.put("MT", "Helena");
        stateCodeToCapitalMap.put("NC", "Raleigh");
        stateCodeToCapitalMap.put("ND", "Bismarck");
        stateCodeToCapitalMap.put("NE", "Lincoln");
        stateCodeToCapitalMap.put("NH", "Concord");
        stateCodeToCapitalMap.put("NJ", "Trenton");
        stateCodeToCapitalMap.put("NM", "Santa Fe");
        stateCodeToCapitalMap.put("NV", "Carson City");
        stateCodeToCapitalMap.put("NY", "Albany");
        stateCodeToCapitalMap.put("OH", "Columbus");
        stateCodeToCapitalMap.put("OK", "Oklahoma City");
        stateCodeToCapitalMap.put("OR", "Salem");
        stateCodeToCapitalMap.put("PA", "Harrisburg");
        stateCodeToCapitalMap.put("RI", "Providence");
        stateCodeToCapitalMap.put("SC", "Columbia");
        stateCodeToCapitalMap.put("SD", "Pierre");
        stateCodeToCapitalMap.put("TN", "Nashville");
        stateCodeToCapitalMap.put("TX", "Austin");
        stateCodeToCapitalMap.put("UT", "Salt Lake City");
        stateCodeToCapitalMap.put("VA", "Richmond");
        stateCodeToCapitalMap.put("VT", "Montpelier");
        stateCodeToCapitalMap.put("WA", "Olympia");
        stateCodeToCapitalMap.put("WI", "Madison");
        stateCodeToCapitalMap.put("WV", "Charleston");
        stateCodeToCapitalMap.put("WY", "Cheyenne");
        stateCodeToCapitalMap.put("VI", "Virgin Islands");
    }
}
